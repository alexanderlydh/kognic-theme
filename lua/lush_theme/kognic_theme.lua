--
-- Built with,
--
--        ,gggg,
--       d8" "8I                         ,dPYb,
--       88  ,dP                         IP'`Yb
--    8888888P"                          I8  8I
--       88                              I8  8'
--       88        gg      gg    ,g,     I8 dPgg,
--  ,aa,_88        I8      8I   ,8'8,    I8dP" "8I
-- dP" "88P        I8,    ,8I  ,8'  Yb   I8P    I8
-- Yb,_,d88b,,_   ,d8b,  ,d8b,,8'_   8) ,d8     I8,
--  "Y8P"  "Y888888P'"Y88P"`Y8P' "YY8P8P88P     `Y8
--

-- This is a starter colorscheme for use with Lush,
-- for usage guides, see :h lush or :LushRunTutorial

--
-- Note: Because this is a lua file, vim will append it to the runtime,
--       which means you can require(...) it in other lua code (this is useful),
--       but you should also take care not to conflict with other libraries.
--
--       (This is a lua quirk, as it has somewhat poor support for namespacing.)
--
--       Basically, name your file,
--
--       "super_theme/lua/lush_theme/super_theme_dark.lua",
--
--       not,
--
--       "super_theme/lua/dark.lua".
--
--       With that caveat out of the way...
--

-- Enable lush.ify on this file, run:
--
--  `:Lushify`
--
--  or
--
--  `:lua require('lush').ify()`

local lush = require("lush")
local hsl = lush.hsl

-- New kognic theme colors
-- Primary colors (comment is which color it commonly replaces)
local black = hsl("#000000") -- black
local dark = hsl("#0d0d0d") -- annotellDark
local primary90 = hsl("#1a1a1a") -- shadow97
local primary87 = hsl("#212121") -- shadow90
local primary80 = hsl("#333333") -- shadow80
local primary88 = hsl("#262626") -- Does not exist) this is guesswork
local primary75 = hsl("#404040") -- shadow75
local primary70 = hsl("#4d4d4d") -- shadow70
local primary55 = hsl("#737373") -- tint30
local primary40 = hsl("#999999") -- tint50 & tint60
local primary20 = hsl("#cccccc") -- tint80
local white = hsl("#ffffff") -- white

-- Main UI accent colors
local kiwiDarker = hsl("#136618")
local kiwiDark = hsl("#46ab4b")
local kiwi = hsl("#83fc89")
local kiwiLight = hsl("#b6fcb9")
local kiwiLighter = hsl("#d7fcd8")
local lavenderDarker = hsl("#2f248c")
local lavenderDark = hsl("#5145bf")
local lavender = hsl("#988cff")
local lavenderLight = hsl("#bfb8ff")
local lavenderLighter = hsl("#ddd9ff")
local blushDarker = hsl("#a81171")
local blushDark = hsl("#e866b8")
local blushMedium = hsl("#f797d4")
local blush = hsl("#f7c8e6")
local blushLight = hsl("#ffe8f6")
local watermelonDarker = hsl("#910f0f")
local watermelonDark = hsl("#ab2222")
local watermelon = hsl("#ff6161")
local watermelonLight = hsl("#ff9999")
local watermelonLighter = hsl("#ffcccc")
-- Addition accent colors
local skyDarker = hsl("#0057ad")
local skyDark = hsl("#4599ed")
local sky = hsl("#85c2ff")
local skyLight = hsl("#b8dbff")
local skyLighter = hsl("#d9ecff")
local tangerineDarker = hsl("#914100")
local tangerineDark = hsl("#e37b27")
local tangerine = hsl("#ffa963")
local tangerineLight = hsl("#ffc596")
local tangerineLighter = hsl("#ffe2c9")
local lemonadeDarker = hsl("#d6a400")
local lemonadeDark = hsl("#f0c330")
local lemonade = hsl("#fad764")
local lemonadeLight = hsl("#fae8af")
local lemonadeLighter = hsl("#fff5d6")
local background = black

-- LSP/Linters mistakenly show `undefined global` errors in the spec, they may
-- support an annotation like the following. Consult your server documentation.
---@diagnostic disable: undefined-global
local theme = lush(function(injected_functions)
  local sym = injected_functions.sym
    return {
      -- The following are the Neovim (as of 0.8.0-dev+100-g371dfb174) highlight
      -- groups, mostly used for styling UI elements.
      -- Comment them out and add your own properties to override the defaults.
      -- An empty definition `{bg=black, fg=white}` will clear all styling, leaving elements looking
      -- like the 'Normal' group.
      -- To be able to link to a group, it must already be defined, so you may have
      -- to reorder items as you go.
      --
      -- See :h highlight-groups
      --
      -- ColorColumn  {bg=black, fg=white}, -- Columns set with 'colorcolumn'
      -- Conceal      {bg=black, fg=white}, -- Placeholder characters substituted for concealed text (see 'conceallevel')
      -- Cursor       {bg=black, fg=white}, -- Character under the cursor
      -- lCursor      {bg=black, fg=white}, -- Character under the cursor when |language-mapping| is used (see 'guicursor')
      -- CursorIM     {bg=black, fg=white}, -- Like Cursor, but used when in IME mode |CursorIM|
      -- CursorColumn {bg=black, fg=white}, -- Screen-column at the cursor, when 'cursorcolumn' is set.
      CursorLine   {bg=primary90}, -- Screen-line at the cursor, when 'cursorline' is set. Low-priority if foreground (ctermfg OR guifg) is not set.
      -- Directory    {bg=black, fg=white}, -- Directory names (and other special names in listings)
      DiffAdd      {fg=kiwi}, -- Diff mode: Added line |diff.txt|
      DiffChange   {fg=lavender}, -- Diff mode: Changed line |diff.txt|
      DiffDelete   {fg=watermelon}, -- Diff mode: Deleted line |diff.txt|
      -- DiffText     {bg=black, fg=white}, -- Diff mode: Changed text within a changed line |diff.txt|
      -- EndOfBuffer  {bg=black, fg=white}, -- Filler lines (~) after the end of the buffer. By default, this is highlighted like |hl-NonText|.
      -- TermCursor   {bg=black, fg=white}, -- Cursor in a focused terminal
      -- TermCursorNC {bg=black, fg=white}, -- Cursor in an unfocused terminal
      ErrorMsg     {bg=black, fg=watermelonDark}, -- Error messages on the command line
      -- VertSplit    {bg=black, fg=white}, -- Column separating vertically split windows
      -- Folded       {bg=black, fg=white}, -- Line used for closed folds
      -- FoldColumn   {bg=black, fg=white}, -- 'foldcolumn'
      SignColumn   {bg=black, fg=white}, -- Column where |signs| are displayed
      IncSearch    {fg=blush}, -- 'incsearch' highlighting; also used for the text replaced with ":s///c"
      -- Substitute   {bg=black, fg=white}, -- |:substitute| replacement text highlighting
      LineNr       {bg=black, fg=primary55}, -- Line number for ":number" and ":#" commands, and when 'number' or 'relativenumber' option is set.
      CursorLineNr {bg=black, fg=white}, -- Like LineNr when 'cursorline' or 'relativenumber' is set for the cursor line.
      MatchParen   {bg=primary80, fg=white}, -- Character under the cursor or just before it, if it is a paired bracket, and its match. |pi_paren.txt|
      ModeMsg      {bg=kiwiDark, fg=black}, -- 'showmode' message (e.g., "-- INSERT -- ")
      -- MsgArea      {bg=black, fg=white}, -- Area for messages and cmdline
      -- MsgSeparator {bg=black, fg=white}, -- Separator for scrolled messages, `msgsep` flag of 'display'
      -- MoreMsg      {bg=black, fg=white}, -- |more-prompt|
      -- NonText      {bg=black, fg=white}, -- '@' at the end of the window, characters from 'showbreak' and other characters that do not really exist in the text (e.g., ">" displayed when a double-wide character doesn't fit at the end of the line). See also |hl-EndOfBuffer|.
      Normal       {bg=black, fg=white}, -- Normal text
      -- NormalFloat  {bg=black, fg=white}, -- Normal text in floating windows.
      -- NormalNC     {bg=black, fg=white}, -- normal text in non-current windows
      Pmenu        {bg=primary90, fg=blush}, -- Popup menu: Normal item.
      PmenuSel     {bg=primary70, fg=sky}, -- Popup menu: Selected item.
      PmenuSbar    {bg=primary90, fg=skyDark}, -- Popup menu: Scrollbar.
      PmenuThumb   {bg=primary90, fg=white}, -- Popup menu: Thumb of the scrollbar.
      Question     {bg=tangerineDark, fg=white}, -- |hit-enter| prompt and yes/no questions
      -- QuickFixLine {bg=black, fg=white}, -- Current |quickfix| item in the quickfix window. Combined with |hl-CursorLine| when the cursor is there.
      Search       {bg=primary70 ,fg=lavenderLighter}, -- Last search pattern highlighting (see 'hlsearch'). Also used for similar items that need to stand out.
      -- SpecialKey   {bg=black, fg=white}, -- Unprintable characters: text displayed differently from what it really is. But not 'listchars' whitespace. |hl-Whitespace|
      -- SpellBad     {bg=black, fg=white}, -- Word that is not recognized by the spellchecker. |spell| Combined with the highlighting used otherwise.
      -- SpellCap     {bg=black, fg=white}, -- Word that should start with a capital. |spell| Combined with the highlighting used otherwise.
      -- SpellLocal   {bg=black, fg=white}, -- Word that is recognized by the spellchecker as one that is used in another region. |spell| Combined with the highlighting used otherwise.
      -- SpellRare    {bg=black, fg=white}, -- Word that is recognized by the spellchecker as one that is hardly ever used. |spell| Combined with the highlighting used otherwise.
      StatusLine   {bg=kiwiDark, fg=black}, -- Status line of current window
      StatusLineNC {bg=primary80, fg=white}, -- Status lines of not-current windows. Note: If this is equal to "StatusLine" Vim will use "^^^" in the status line of the current window.
      TabLine      {bg=primary70, fg=white}, -- Tab pages line, not active tab page label
      TabLineFill  {bg=primary80, fg=white}, -- Tab pages line, where there are no labels
      TabLineSel   {bg=kiwiDark, fg=black}, -- Tab pages line, active tab page label
      -- Title        {bg=black, fg=white}, -- Titles for output from ":set all", ":autocmd" etc.
      Visual       {bg=primary70}, -- Visual mode selection
      VisualNOS    {bg=lavenderDarker}, -- Visual mode selection when vim is "Not Owning the Selection".
      WarningMsg   {bg=black, fg=lemonade}, -- Warning messages
      -- Whitespace   {bg=black, fg=white}, -- "nbsp", "space", "tab" and "trail" in 'listchars'
      Winseparator {bg=primary80, fg=primary80}, -- Separator between window splits. Inherts from |hl-VertSplit| by default, which it will replace eventually.
      -- WildMenu     {bg=black, fg=white}, -- Current match in 'wildmenu' completion

      -- Common vim syntax groups used for all kinds of code and markup.
      -- Commented-out groups should chain up to their preferred (*) group
      -- by default.
      --
      -- See :h group-name
      --
      -- Uncomment and edit if you want more specific syntax highlighting.

      Comment        {fg=primary55}, -- Any comment

      Constant       {fg=skyDark}, -- (*) Any constant
      String         {fg=lemonade}, --   A string constant: "this is a string"
      Character      {fg=lemonadeDarker}, --   A character constant: 'c', '\n'
      Number         {fg=lavender}, --   A number constant: 234, 0xff
      Boolean        {fg=skyDark}, --   A boolean constant: TRUE, false
      Float          {fg=lavender}, --   A floating point constant: 2.3e10

      Identifier     {fg=white}, -- (*) Any variable name
      Function       {fg=kiwi}, --   Function name (also: methods for classes)

      Statement      {fg=white}, -- (*) Any statement
      Conditional    {fg=skyDark}, --   if, then, else, endif, switch, etc.
      Repeat         {fg=skyDark}, --   for, do, while, etc.
      Label          {fg=skyDark}, --   case, default, etc.
      Operator       {fg=watermelonDark}, --   "sizeof", "+", "*", etc.
      Keyword        {fg=skyDark}, --   any other keyword
      Exception      {fg=skyDark}, --   try, catch, throw

      PreProc        {fg=white}, -- (*) Generic Preprocessor
      Include        {fg=white}, --   Preprocessor #include
      Define         {fg=white}, --   Preprocessor #define
      Macro          {fg=white}, --   Same as Define
      PreCondit      {fg=white}, --   Preprocessor #if, #else, #endif, etc.

      Type           {fg=kiwi}, -- (*) int, long, char, etc.
      StorageClass   {fg=white}, --   static, register, volatile, etc.
      Structure      {fg=white}, --   struct, union, enum, etc.
      Typedef        {fg=white}, --   A typedef

      Special        {fg=white}, -- (*) Any special symbol
      SpecialChar    {fg=white}, --   Special character in a constant
      Tag            {fg=white}, --   You can use CTRL-] on this
      Delimiter      {fg=white}, --   Character that needs attention
      SpecialComment {fg=white}, --   Special things inside a comment (e.g. '\n')
      Debug          {fg=white}, --   Debugging statements

      Underlined     { gui = "underline" }, -- Text that stands out, HTML links
      Bold           { gui = "bold" },
      Italic         { gui = "italic" },
      Ignore         {fg=white}, -- Left blank, hidden |hl-Ignore| (NOTE: May be invisible here in template)
      Error          {fg=watermelonDark}, -- Any erroneous construct
      Todo           {fg=watermelonLight}, -- Anything that needs extra attention; mostly the keywords TODO FIXME and XXX

      -- These groups are for the native LSP client and diagnostic system. Some
      -- other LSP clients may use these groups, or use their own. Consult your
      -- LSP client's documentation.

      -- See :h lsp-highlight, some groups may not be listed, submit a PR fix to lush-template!
      --
      LspReferenceText            {bg=black, fg=white} , -- Used for highlighting "text" references
      LspReferenceRead            {bg=black, fg=white} , -- Used for highlighting "read" references
      LspReferenceWrite           {bg=black, fg=white} , -- Used for highlighting "write" references
      LspCodeLens                 {bg=black, fg=white} , -- Used to color the virtual text of the codelens. See |nvim_buf_set_extmark()|.
      LspCodeLensSeparator        {bg=black, fg=white} , -- Used to color the seperator between two or more code lens.
      LspSignatureActiveParameter {bg=black, fg=white} , -- Used to highlight the active parameter in the signature help. See |vim.lsp.handlers.signature_help()|.

      -- See :h diagnostic-highlights, some groups may not be listed, submit a PR fix to lush-template!
      --
      -- DiagnosticError            {fg=white} , -- Used as the base highlight group. Other Diagnostic highlights link to this by default (except Underline)
      -- DiagnosticWarn             {fg=white} , -- Used as the base highlight group. Other Diagnostic highlights link to this by default (except Underline)
      -- DiagnosticInfo             {fg=white} , -- Used as the base highlight group. Other Diagnostic highlights link to this by default (except Underline)
      -- DiagnosticHint             {fg=white} , -- Used as the base highlight group. Other Diagnostic highlights link to this by default (except Underline)
      DiagnosticVirtualTextError {fg=watermelonDark} , -- Used for "Error" diagnostic virtual text.
      DiagnosticVirtualTextWarn  {fg=lemonade} , -- Used for "Warn" diagnostic virtual text.
      DiagnosticVirtualTextInfo  {fg=lavender} , -- Used for "Info" diagnostic virtual text.
      DiagnosticVirtualTextHint  {fg=kiwiLight} , -- Used for "Hint" diagnostic virtual text.
      -- DiagnosticUnderlineError   {fg=white} , -- Used to underline "Error" diagnostics.
      -- DiagnosticUnderlineWarn    {fg=white} , -- Used to underline "Warn" diagnostics.
      -- DiagnosticUnderlineInfo    {fg=white} , -- Used to underline "Info" diagnostics.
      -- DiagnosticUnderlineHint    {fg=white} , -- Used to underline "Hint" diagnostics.
      DiagnosticFloatingError    {fg=watermelonDark} , -- Used to color "Error" diagnostic messages in diagnostics float. See |vim.diagnostic.open_float()|
      DiagnosticFloatingWarn     {fg=lemonade} , -- Used to color "Warn" diagnostic messages in diagnostics float.
      DiagnosticFloatingInfo     {fg=lavender} , -- Used to color "Info" diagnostic messages in diagnostics float.
      DiagnosticFloatingHint     {fg=kiwiLight} , -- Used to color "Hint" diagnostic messages in diagnostics float.
      DiagnosticSignError        {fg=watermelonDark} , -- Used for "Error" signs in sign column.
      DiagnosticSignWarn         {fg=lemonade} , -- Used for "Warn" signs in sign column.
      DiagnosticSignInfo         {fg=lavender} , -- Used for "Info" signs in sign column.
      DiagnosticSignHint         {fg=kiwiLight} , -- Used for "Hint" signs in sign column.

      -- Tree-Sitter syntax groups. Most link to corresponding
      -- vim syntax groups (e.g. TSKeyword => Keyword) by default.
      --
      -- See :h nvim-treesitter-highlights, some groups may not be listed, submit a PR fix to lush-template!
      sym("@annotation")         {fg=white},
      sym("@attribute")          {fg=white} , -- Annotations that can be attached to the code to denote some kind of meta information. e.g. C++/Dart attributes.
      sym("@boolean")            {fg=skyDark} , -- Boolean literals: `True` and `False` in Python.
      sym("@character")          {fg=lemonadeDarker} , -- Character literals: `'a'` in C.
      sym("@characterspecial")   {fg=white} , -- Special characters.
      sym("@comment")            {fg=primary55} , -- Line comments and block comments.
      sym("@conditional")        {fg=skyDark} , -- Keywords related to conditionals: `if`, `when`, `cond`, etc.
      sym("@constant")           {fg=white} , -- Constants identifiers. These might not be semantically constant. E.g. uppercase variables in Python.
      sym("@constant.builtin")   {fg=skyDark},
      sym("@constant.macro")     {fg=white},
      sym("@constructor")        {fg=skyDark} , -- Constructor calls and definitions: `{bg=black, fg=white}` in Lua, and Java constructors.
      sym("@danger")             {fg=white} , -- Text representation of a danger note.
      sym("@debug")              {fg=white} , -- Debugging statements.
      sym("@define")             {fg=white} , -- Preprocessor #define statements.
      sym("@emphasis")           {fg=white} , -- Text to be represented with emphasis.
      sym("@environment")        {fg=white} , -- Text environments of markup languages.
      sym("@environment.name")   {fg=white} , -- Text/string indicating the type of text environment. Like the name of a `\begin` block in LaTeX.
      sym("@error")              {fg=white} , -- Syntax/parser errors. This might highlight large sections of code while the user is typing still incomplete code, use a sensible highlight.
      sym("@exception")          {fg=skyDark} , -- Exception related keywords: `try`, `except`, `finally` in Python.
      sym("@field")              {fg=white} , -- Object and struct fields.
      sym("@float")              {fg=lavender} , -- Floating-point number literals.
      sym("@function")           {fg=kiwi} , -- Function calls and definitions.
      sym("@function.builtin")   {fg=skyDark},
      sym("@function.call")      {fg=skyDark},
      sym("@function.macro")     {fg=white},
      sym("@include")            {fg=skyDark} , -- File or module inclusion keywords: `#include` in C, `use` or `extern crate` in Rust.
      sym("@keyword")            {fg=blushDark} , -- Keywords that don't fit into other categories.
      sym("@keyword.function")   {fg=skyDark},
      sym("@keyword.operator")   {fg=skyDark} , -- Unary and binary operators that are English words: `and`, `or` in Python; `sizeof` in C.
      sym("@keyword.return")     {fg=skyDark} , -- Keywords like `return` and `yield`.
      sym("@label")              {fg=white} , -- GOTO labels: `label:` in C, and `::label::` in Lua.
      sym("@literal")            {fg=lemonade} , -- Literal or verbatim text.
      sym("@math")               {fg=white} , -- Math environments like LaTeX's `$ ... $`
      sym("@method")             {fg=kiwi} , -- Method calls and definitions.
      sym("@method.call")        {fg=skyDark} , -- Method calls and definitions.
      sym("@namespace")          {fg=skyDark} , -- Identifiers referring to modules and namespaces.
      sym("@none")               {fg=white} , -- No highlighting (sets all highlight arguments to `NONE`). this group is used to clear certain ranges, for example, string interpolations. Don't change the values of this highlight group.
      sym("@note")               {fg=white} , -- Text representation of an informational note.
      sym("@number")             {fg=lavender} , -- Numeric literals that don't fit into other categories.
      sym("@operator")           {fg=watermelonDark} , -- Binary or unary operators: `+`, and also `->` and `*` in C.
      sym("@parameter")          {fg=tangerineDark} , -- Parameters of a function.
      sym("@parameter.reference") {fg=tangerineDark} , -- References to parameters of a function.
      sym("@preproc")            {fg=white} , -- Preprocessor #if, #else, #endif, etc.
      sym("@property")           {fg=lavender} , -- Same as `TSField`.
      sym("@punctuation.bracket") {fg=white },
      sym("@punctuation.delimiter") {fg=white },
      sym("@punctuation.special") {fg=blush  },
      sym("@repeat")             {fg=skyDark} , -- Keywords related to loops: `for`, `while`, etc.
      sym("@storageclass")       {fg=blushDark} , -- Keywords that affect how a variable is stored: `static`, `comptime`, `extern`, etc.
      sym("@strike")             {fg=white} , -- Strikethrough text.
      sym("@string")             {fg=lemonade},
      sym("@string.escape")       {fg=blushDarker} , -- Escape characters within a string: `\n`, `\t`, etc.
      sym("@string.regex")        {fg=kiwiDarker} , -- Regular expression literals.
      sym("@string.special")      {fg=white} , -- Strings with special meaning that don't fit into the previous categories.
      sym("@strong")             {fg=white} , -- Text to be represented in bold.
      sym("@structure")         {fg=sky},
      sym("@symbol")             {fg=white} , -- Identifiers referring to symbols or atoms.
      sym("@tag")                {fg=white} , -- Tags like HTML tag names.
      sym("@tag.attribute")       {fg=white} , -- HTML tag attributes.
      sym("@tag.delimiter")       {fg=white} , -- Tag delimiters like `<` `>` `/`.
      sym("@text")               {fg=white} , -- Non-structured text. Like text in a markup language.
      sym("@text.reference")      {fg=white} , -- Footnotes, text references, citations, etc.
      sym("@title")              {fg=white} , -- Text that is part of a title.
      sym("@type")               {fg=white} , -- Type (and class) definitions and annotations.
      sym("@type.builtin")        {fg=white} , -- Built-in types: `i32` in Rust.
      sym("@underline") { fg = nil, bg = nil, gui = "underline" },
      sym("@uri")                {fg=white} , -- URIs like hyperlinks or email addresses.
      sym("@variable")           {fg=white} , -- Variable names that don't fit into other categories.
      sym("@variable.builtin")    {fg=lavender} , -- Variable names defined by the language: `this` or `self` in Javascript.
      sym("@warning")            {fg=white} , -- Text representation of a warning note.

      CmpItemAbbr {fg=blush}, --Highlight group for unmatched characters of each completion field.
      CmpItemAbbrMatch {fg=skyDark}, --Highlight group for matched characters of each completion field. Matched characters must form a substring of a field which share a starting position.           
      CmpItemAbbrMatchFuzzy {fg=skyDark}, -- Highlight group for fuzzy-matched characters of each completion field.     
      CmpItemKind {fg=white}, -- Highlight group for the kind of the field.
      CmpItemMenu {fg=kiwi}, --  The menu field's highlight group. 

      TelescopeSelection {fg=sky},
      TelescopeSelectionCaret {fg=kiwi},
      TelescopeMultiSelection {TelescopeSelection},
      TelescopeNormal {},
      TelescopeBorder {},
      TelescopePromptBorder {},
      TelescopeResultsBorder {},
      TelescopePreviewBorder {},
      TelescopeMatching {fg=skyDark},
      TelescopePromptPrefix {},
      TelescopePrompt {},

      diffAdded{DiffAdd},
      diffChanged{DiffChange},
      diffRemoved{DiffDelete},
      diffLine{fg=lemonade},

      fugitiveUnstagedModifier{DiffChange}

    }
  end
)

-- Return our parsed t  heme for extension or use elsewhere.
return theme

-- vi:nowrap
